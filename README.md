# Projeto SO
INTEGRANTES:

Ricardo Lino / RA: 21906893

Matheus Vieira / RA: 21903948

EXECUÇÃO:

1º Entre no terminal

2ª Linux ou Windows

2.1º Linux
    
    - Crie um pasta (mkdir) ou entre dentro de uma pasta já existente
    - Dentro da pasta insira o comando git clone https://gitlab.com/ricardolino/projeto-so.git
    - Execute o programa escrevendo: python Traferência_Threads.py ou python3 Transferência_Threads.py 
2.2º Windows
    
    - Dentro do terminal insira o comando git clone https://gitlab.com/ricardolino/projeto-so.git
    - Procura o executavel python e o programa Transferência_Threads em seu computador
    - Inserir o endereço do python em seguida o endereço do programa Transferência_Threads 
    - Pressionar <Enter> que o programa deverá rodar
