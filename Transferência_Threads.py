import _thread

sair_lup = [False] * 100

class banco(object):
    def __init__(self, n, s = 100):
        self.nome = n
        self.saldo = s

def tranferencia(idthread, valor_transferencia, trava):
    trava.acquire()
    if conta1.saldo >= valor_transferencia:
        conta1.saldo -= valor_transferencia
        conta2.saldo += valor_transferencia
        print(f'\nNome: {conta1.nome} / Saldo: {conta1.saldo}'
              f'\nNome: {conta2.nome} / Saldo: {conta2.saldo}')
        trava.release()
        sair_lup[idthread] = True

if __name__ == '__main__':
    conta1 = banco('João')
    conta2 = banco('Maria')

    print(f'Nome: {conta1.nome} / Saldo: {conta1.saldo}'
          f'\nNome: {conta2.nome} / Saldo: {conta2.saldo}')

    chave = _thread.allocate_lock()
    for i in range(100):
        _thread.start_new_thread(tranferencia, (i, 1, chave))

    while False in sair_lup: pass
    print('\n\nTERMINOU')

